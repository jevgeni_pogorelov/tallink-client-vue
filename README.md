# README #

### Client for Conference management system ###

* Create and cancel conferences
* Check conference room availability
* Add remove participants from conference

### Used technologies ###

* [Vue.js](https://vuejs.org)
* [BootstrapVue](https://bootstrap-vue.org)
* [VeeValidate](http://vee-validate.logaretm.com/v2/)
* [Vue I18n](https://kazupon.github.io/vue-i18n/)
* [axios](https://github.com/axios/axios)
* [Vue Router](https://router.vuejs.org)

### Install ###
```.bash
npm clean install
```

### Run ###
```.bash
npm run serve
```
### Usage ###

```.url
http://localhost:8080/
```
