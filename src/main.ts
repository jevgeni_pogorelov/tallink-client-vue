import Vue from 'vue';
import i18n from './i18n';
import VeeValidate from 'vee-validate';
import VueIziToast from 'vue-izitoast';
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue';
import router from './router';
import './registerServiceWorker';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import {BaseService} from '@/services/base.service';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'izitoast/dist/css/iziToast.min.css';


Vue.config.productionTip = false;

Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
});

Vue.use(BootstrapVue);
Vue.component('font-awesome-icon', FontAwesomeIcon);
// @ts-ignore
Vue.use(VueIziToast);

BaseService.init();

new Vue({
  i18n,
  router,
  render: (h) => h(App),
}).$mount('#app');
