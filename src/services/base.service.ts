import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

export abstract class BaseService {

    public static init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = process.env.VUE_APP_API_CONTEXT;
    }

    protected readonly api = process.env.VUE_APP_API_CONTEXT;
}
