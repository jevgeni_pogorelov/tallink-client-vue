import axios from 'axios';
import {BaseService} from '@/services/base.service';

class ParticipantConferenceService extends BaseService {

    private static instance: ParticipantConferenceService;

    private constructor() {
        super();
    }

    public static get Instance() {
        return this.instance || (this.instance = new this());
    }

    public addParticipantToConference(params: any) {
        return axios.post(`${this.api}/participantConference/addToConference`, params);
    }

    public removeParticipantFromConference(params: any) {
        return axios.delete(`${this.api}/participantConference/removeFromConference`, {params});
    }
}

export const participantConferenceService = ParticipantConferenceService.Instance;
