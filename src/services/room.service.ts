import axios from 'axios';
import {BaseService} from '@/services/base.service';

class RoomService extends BaseService {

    private static instance: RoomService;

    private constructor() {
        super();
    }

    public static get Instance() {
        return this.instance || (this.instance = new this());
    }

    public listRooms(params: any) {
        return axios.get(`${this.api}/room`, {params});
    }

    public saveRoom(room: any) {
        return axios.post(`${this.api}/room`, room);
    }

    public findAvailableRoomByConferenceId(conferenceId: string) {
        return axios.get(`${this.api}/room/` + conferenceId);
    }

}

export const roomService = RoomService.Instance;
