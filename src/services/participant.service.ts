import axios from 'axios';
import {BaseService} from '@/services/base.service';

class ParticipantService extends BaseService {

    private static instance: ParticipantService;

    private constructor() {
        super();
    }

    public static get Instance() {
        return this.instance || (this.instance = new this());
    }

    public listParticipants(params: any) {
        return axios.get(`${this.api}/participant`, {params});
    }

    public saveParticipant(participant: any) {
        return axios.post(`${this.api}/participant`, participant);
    }

    public getAllParticipantConferences(participantId: string) {
        return axios.get(`${this.api}/participant/participantConferences/` + participantId);
    }

}

export const participantService = ParticipantService.Instance;
