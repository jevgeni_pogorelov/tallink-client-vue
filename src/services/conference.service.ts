import axios from 'axios';
import {BaseService} from '@/services/base.service';

class ConferenceService extends BaseService {

    private static instance: ConferenceService;

    private constructor() {
        super();
    }

    public static get Instance() {
        return this.instance || (this.instance = new this());
    }

    public listConferences(params: any) {
        return axios.get(`${this.api}/conference`, {params});
    }

    public saveConference(conference: any) {
        return axios.post(`${this.api}/conference`, conference);
    }

    public cancelConference(conferenceId: string) {
        return axios.post(`${this.api}/conference/cancel/` + conferenceId);
    }

    public addRoomToConference(addRoomModel: any) {
        return axios.post(`${this.api}/conference/addRoom`, addRoomModel);
    }

    public findAvailableConferencesToParticipate(params: any) {
        return axios.get(`${this.api}/conference/availableToParticipate`, {params});
    }
}

export const conferenceService = ConferenceService.Instance;
