import iziToast from 'izitoast';

const toast = {
    error: (message: string, title = 'Error') => {
        return iziToast.error({
            title,
            message,
            position: 'bottomCenter',
        });
    },
    success: (message: string, title = 'Success') => {
        return iziToast.success({
            title,
            message,
            position: 'bottomCenter',
        });
    },
};

export default toast;
