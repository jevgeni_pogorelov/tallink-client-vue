import Vue from 'vue';
import Router from 'vue-router';
import Conferences from '@/views/Conferences.vue';
import Participants from '@/views/Participants.vue';
import Rooms from '@/views/Rooms.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/conferences',
    },
    {
      path: '/conferences',
      name: 'conferences',
      component: Conferences,
      meta: {
        title: 'Conferences',
      },
    },
    {
      path: '/participants',
      name: 'participants',
      component: Participants,
      meta: {
        title: 'Participants',
      },
    },
    {
      path: '/rooms',
      name: 'rooms',
      component: Rooms,
      meta: {
        title: 'Rooms',
      },
    },
  ],
});

router.beforeEach((to: any, from: any, next: any) => {
  document.title = to.meta.title ? to.meta.title + ' - Tallink' : 'Tallink';
  next();
});

export default router;
