import en from './en.json';
import et from './et.json';
import ru from './ru.json';

export const languages = {
    en, et, ru,
};
