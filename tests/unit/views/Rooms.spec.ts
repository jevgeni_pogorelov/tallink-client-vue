import {createLocalVue, shallowMount} from '@vue/test-utils';
import Rooms from '@/views/Rooms.vue';
import VeeValidate from 'vee-validate';
import BootstrapVue from 'bootstrap-vue';
import i18n from '@/i18n';

describe('Rooms.vue', () => {

    let wrapper: any;

    beforeEach(() => {
        const localVue = createLocalVue();
        localVue.use(VeeValidate);
        localVue.use(BootstrapVue);

        wrapper = shallowMount(Rooms, {
            localVue,
            i18n,
        });
    });
    it('renders a vue instance', () => {
        expect(wrapper.isVueInstance()).toBe(true);
    });
    it('has an h3', () => {
        expect(wrapper.contains('h3')).toBe(true);
    });

});
