import {createLocalVue, shallowMount} from '@vue/test-utils';
import Router from 'vue-router';
import App from '@/App.vue';
import NavBar from '@/components/NavBar.vue';
import BootstrapVue from 'bootstrap-vue';

describe('App.vue', () => {
    let wrapper: any;

    beforeEach(() => {
        const localVue = createLocalVue();
        localVue.use(Router);
        localVue.use(BootstrapVue);

        wrapper = shallowMount(App, {
            localVue,
        });
    });

    it('renders a vue instance', () => {
        expect(wrapper.isVueInstance()).toBe(true);
    });

    it('check if child NavBar exists', () => {
        expect(wrapper.contains(NavBar)).toBe(true);
    });

});
